require 'oauth2'

class OAuth2::Checker
  def initialize(client_id, client_secret, options = {}, &block)
    @client = OAuth2::Client.new(client_id, client_secret, options, &block)
  end

  def check_access_token(token)
    # raise exception if no token exists
    # at = AccessToken.where(token: token).take

    # try to check if token is still valid with oauth provider
    begin
      # try to do a request to the oauth provider
      req = @client.request(:get, "oauth/token/info", {:params => {:access_token => token}})
      # if i got here, the request was success => token is valid
      return token
    rescue
      # unexisting or expired token, access denied
      return false
    end
  end

  def get_resource_owner(token)
    begin
      req = @client.request(:get, "oauth/token/info", {:params => {:access_token => token}})
      return req.parsed["resource_owner_id"]
    rescue
      return "undefined"
    end
  end
end
