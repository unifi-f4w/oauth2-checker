Gem::Specification.new do |s|
  s.name        = 'oauth2-checker'
  s.version     = '0.1.0'
  s.summary     = "OAuth2Checker"
  s.description = "Just a simple OAuth2 token info checker"
  s.authors     = ["Sandro Mehic", "Alessio Caiazza"]
  s.email       = 'devs@facts4.work'
  s.homepage    = 'http://facts4workers.eu/'
  s.files       = ["lib/oauth2/checker.rb"]
  s.add_runtime_dependency 'oauth2', '~> 1.1'
end
