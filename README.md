# OAuth2 Checker

## Installation

Please add the following lines to your ```Gemfile```

```ruby
git_source(:gitlab){ |repo_name| "https://gitlab.com/#{repo_name}.git" }

gem 'oauth2-checker', gitlab: 'unifi-f4w/oauth2-checker', branch: 'master'

```

Then create a new initializer in ```config/initializers/oauth2_cleint.rb```

```ruby
require 'oauth2'

Rails.application.config.x.oauth_options = { site: ENV['OAUTH_PROVIDER_URL'] }
```

And a concern ```app/controllers/concerns/authenticated_controller.rb```

```ruby
module AuthenticatedController
  extend ActiveSupport::Concern

  TEST_VALID_TOKEN = 'valid_token_for_tests'

  included do
    before_action :require_valid_user


    def require_valid_user

      if Rails.env != 'test'
        failed = !oauth2_checker.check_access_token(params[:access_token])
      else
        failed = params[:access_token] != TEST_VALID_TOKEN
      end

      if failed
        render status: :unauthorized, json: {message: 'Unauthorized: invalid or expired access token', code: 401}
      end
    end

    def resource_owner_id
      if Rails.env != 'test'
        oauth2_checker.get_resource_owner(params[:access_token])
      else
        'test_user_uid'
      end
    end

    def oauth2_checker
      OAuth2::Checker.new(
          Rails.application.secrets.client_id,
          Rails.application.secrets.client_secret,
          Rails.application.config.x.oauth_options)
    end
  end
end
```

Finally you had to add some properties in ```config/secrets.yml```

```yml
default: &default
  client_id: <%= ENV["OAUTH_CLIENT_ID"] %>
  client_secret: <%= ENV["OAUTH_CLIENT_SECRET"] %>
  secret_key_base: <%= ENV["SECRET_KEY_BASE"] %>

development:
  <<: *default

test:
  <<: *default

production:
  <<: *default

staging:
  <<: *default
```

## Usage

Every controller you need to protect with OAuth must include the created concern.

```ruby
class Api::V1::MyProtectedController < ApplicationController
  include AuthenticatedController

  #
  # all your code
  #
end
```
